<?php

namespace Masteryodo\MagentoModule\Block;

use Magento\Cron\Model\Schedule;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Masteryodo\MagentoModule\Service\CronService;

class Block extends Template
{

    /**
     * @var CronService
     */
    private $_cronService;

    public function __construct(
    Context $context,
    CronService $cronService,
    array $data = []
   ){
    parent::__construct($context, $data);
    $this->_cronService = $cronService;
  }
    
  
    public function getPreparedData() {
        $schedules = $this->_cronService->getSchedules();

        $scheduleData = [];
        /** @var Schedule $schedule */
        foreach ($schedules as $schedule) {
            $scheduleData[$schedule->getid()] = $schedule->getJobCode() ." ". $schedule->getStatus();
        }
        
        return $scheduleData;
    }
}