<?php
namespace Masteryodo\MagentoModule\Api;

use Magento\Framework\Controller\Result\Json;

/**
 * Interface CronServiceInterface
 * @api
 */
interface CronServiceInterface
{
    /**
     * @return Json
     */
    public function getAllSchedules();

    /**
     * @param string[] $data
     * @return Json
     */
    public function createScheduleFromJson($data);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);
}