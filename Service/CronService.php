<?php

namespace Masteryodo\MagentoModule\Service;

use Exception;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\StateException;
use Masteryodo\MagentoModule\Api\CronServiceInterface;
use Magento\Cron\Model\ScheduleFactory;
use Magento\Cron\Model\Schedule;
use Magento\Cron\Model\ResourceModel\Schedule\Collection;

class CronService implements CronServiceInterface
{
    /**
     * @var ScheduleFactory
     */
    protected $_scheduleFactory;
    protected $_scheduleCollection;

    public function __construct(
        ScheduleFactory $scheduleFactory,
        Collection $scheduleCollection
    )
    {
        $this->_scheduleFactory = $scheduleFactory;
        $this->_scheduleCollection = $scheduleCollection;
    }

    /**
     * @param string[] $data
     * @return Schedule
     * @throws Exception
     */
    public function createScheduleFromJson($data)
    {
        $schedule = $this->_scheduleFactory->create();
        $schedule->setId(null);
        $schedule->setJobCode($data['job_code'])->setScheduledAt($data['scheduled_at']); //TODO need to validate time format
        $scheduleResource = $schedule->getResource();

        try {
            $scheduleResource->save($schedule);
        } catch (\Exception $e) {
            throw new Exception(__('Unable to save Schedule #%1', $schedule->getId()));
        }
        return $schedule->getData();
    }

    /**
     * @param int $id
     * @return bool
     * @throws StateException
     * @throws NotFoundException
     */
    public function deleteById($id)
    {
        $schedule = $this->_scheduleFactory->create();
        $schedule->getResource()->load($schedule, $id);

        if ($schedule->getId() != null) {
            try {
                $schedule->getResource()->delete($schedule);
            } catch (\Exception $e) {
                throw new StateException(__('Unable to remove schedule #%1', $schedule->getId()));
            }
            return true;
        } else {
            throw  new NotFoundException(__('Unable to find schedule with id', $id));
        }
    }

    /**
     * @return array
     */
    public function getAllSchedules()
    {
        return $this->_scheduleCollection->getData();
    }


    public function getSchedules()
    {
        return $this->_scheduleCollection;
    }

}