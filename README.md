# magento-module

##### Module main page

`{your-server-ip}/magentomodule`

##### Install with composer
`````
composer config repositories.masteryodo git git@gitlab.com:masteryodo/magento-module.git
composer require masteryodo dev-master
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
php bin/magento cache:clean
`````


##### REST API

GET - returns all schedules

`{your-server-ip}/rest/all/V1/schedule`

POST - create new one schedule

`{your-server-ip}/rest/all/V1/schedule`

payload example

````
{ "data": {
        "job_code": "some job code",
        "scheduled_at": "2019-10-13 19:53:56" 
        }
}
````


DELETE - delete one of schedule by it`s id

`{your-server-ip}/rest/all/V1/schedule/{schedule-id}`


##### common commands
- bin/magento cache:disable
- bin/magento deploy:mode:set developer
- bin/magento setup:upgrade
- bin/magento setup:static-content:deploy  - optional

check
- bin/magento module:status